//
//  ViewController.swift
//  HelloWorldSwift
//
//  Created by Wepsys_dev on 4/3/19.
//  Copyright © 2019 Wepsys_dev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func sayHello(_ sender: AnyObject){
        label.text = "Hello, do you like my hat?"
    }
    
}

